import React from "react";
import Todo from "./components/Todo";
import "./todo.css";

function App() {
  return (
    <div>
      <Todo />
    </div>
  );
}

export default App;
