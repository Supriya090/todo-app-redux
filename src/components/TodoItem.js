import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "../todo.css";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { useDispatch } from "react-redux";
import { toggleComplete, deleteTodo } from "../redux/todoSlice";

function TodoItem({ id, completed, title }) {
  const dispatch = useDispatch();

  const handleComplete = () => {
    dispatch(toggleComplete({ id: id, completed: !completed }));
  };

  const handleDelete = () => {
    dispatch(deleteTodo({ id: id }));
  };

  return (
    <div key={id} className='todoRow'>
      <div
        style={{
          textDecoration: completed ? "line-through" : "",
          color: completed ? "black" : "",
        }}>
        {title}
        {console.log(completed)}
      </div>
      <div className='icons'>
        <FontAwesomeIcon icon={faTrash} onClick={handleDelete} />
        <label className='container'>
          <input
            type='checkbox'
            onClick={() => {
              handleComplete();
              console.log(completed);
            }}
            checked={completed ? "checked" : ""}
          />
          <span className='checkmark'></span>
        </label>
      </div>
    </div>
  );
}

export default TodoItem;
