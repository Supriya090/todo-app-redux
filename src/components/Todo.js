import React from "react";
import Form from "./Form";
import "../todo.css";
import { useSelector } from "react-redux";
import TodoItem from "./TodoItem";
import CompleteTodos from "./CompleteTodos";

function Todo() {
  const todos = useSelector((state) => state.todos);

  return (
    <>
      <div className='todoH1'>
        <h1>TO-DO LIST</h1>
      </div>
      <div className='todoClass'>
        <h2 className='todoHead'> Hey! What are your plans today?</h2>
        <Form />
        <div className='todoContainer'>
          {todos.map((todo) => (
            <TodoItem
              id={todo.id}
              completed={todo.completed}
              title={todo.title}
            />
          ))}
        </div>
        <CompleteTodos />
      </div>
    </>
  );
}

export default Todo;
