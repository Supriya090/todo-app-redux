import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addTodo } from "../redux/todoSlice";

function Form({ onSubmit }) {
  const [value, setValue] = useState("");

  const dispatch = useDispatch();

  return (
    <form
      onSubmit={(e) => {
        e.preventDefault();
        dispatch(
          addTodo({
            title: value,
          })
        );
        setValue("");
      }}>
      <input
        type='text'
        placeholder='Enter a task'
        className='todoInput'
        value={value}
        onChange={(e) => setValue(e.target.value)}></input>
      <button className='addButton'>Add Task</button>
    </form>
  );
}

export default Form;
