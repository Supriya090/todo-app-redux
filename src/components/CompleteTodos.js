import React from "react";
import { useSelector } from "react-redux";

function CompleteTodos() {
  const completedTodos = useSelector((state) =>
    state.todos.filter((todo) => todo.completed === false)
  );
  return (
    <div>
      {!completedTodos.length ? (
        <h3>Wohoo! No tasks remaining!</h3>
      ) : completedTodos.length === 1 ? (
        <h3>Only one task to go!</h3>
      ) : (
        <h3>{completedTodos.length} tasks remaining!</h3>
      )}
      <button
        className='clear'
        onClick={() => {
          localStorage.clear();
          window.location.reload(false);
        }}>
        Clear
      </button>
    </div>
  );
}

export default CompleteTodos;
