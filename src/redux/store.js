import { configureStore } from "@reduxjs/toolkit";
import { loadState, saveState } from "../localStorage/localStorage";
import todoReducer from "./todoSlice";
import throttle from "lodash/throttle";

const preloadedState = loadState();
const reducer = {
  todos: todoReducer,
};

const store = configureStore({
  reducer,
  preloadedState,
});

store.subscribe(
  throttle(() => {
    saveState(store.getState());
  }, 1000)
);

export default store;
